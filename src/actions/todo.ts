import { ITodo } from "../reducers/todo";
import { ThunkAction } from "redux-thunk";
import { Action } from "redux";
import {IStore} from "../reducers";

export enum TodoActionTypes {
    LOAD = 'TODO.LOAD',
}

export const loadTodos = ():ThunkAction<Promise<void>,IStore,null,Action<string>> => (dispatch) => {
    return new Promise<void>((resolve,reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET','https://jsonplaceholder.typicode.com/todos');
        xhr.onload = () => {
            if(xhr.status >= 300) return reject();
            dispatch({
                type:TodoActionTypes.LOAD,
                todo: JSON.parse(xhr.response)
            });
            return resolve()
        };
        xhr.onerror = () => reject();
        xhr.send();
    })

};

export type TodoActions = {
    LOAD: {
        type: typeof TodoActionTypes.LOAD,
        todo: ITodo[]
    }
}

export type ITodoActions = TodoActions[keyof TodoActions];
