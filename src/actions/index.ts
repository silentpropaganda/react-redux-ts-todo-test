import {IPageStateActions} from "./pageState";
import {ITodoActions} from "./todo";

export type IActions = IPageStateActions | ITodoActions

export type Dispatch = (action: IActions) => void;