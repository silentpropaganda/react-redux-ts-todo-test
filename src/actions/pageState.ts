import {Dispatch} from "./index";
import {store} from "../index";

export enum PageStateActionTypes {
    SET = 'PAGE_STATE.SET'
}

export const selectTodo = (id:any,push:boolean = true) => (dispatch:Dispatch, state: typeof store.getState) => {
    push && history.pushState(
        {},
        `Todo ${id}`,
        id === null ? `/` : `/#todo=${id}`
        );
    dispatch({type:PageStateActionTypes.SET,path:['selectedTodo'],data:id ? parseInt(id) : null})
};

export type PageStateActions = {
    SET: {
        type: typeof PageStateActionTypes.SET,
        path: string[],
        data: any
    }
};

export const setPageState = (path:string[],data:any) => ({
    type: PageStateActionTypes.SET,
    path,
    data
});

export type IPageStateActions = PageStateActions[keyof  PageStateActions]