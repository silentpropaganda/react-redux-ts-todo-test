export const getHash = function(hash:string) {
    let hash_obj:any = {};
    hash = hash.replace(/#/,'');
    hash = hash.replace(/\?/,'');
    if (hash !== "") {
        let hash_arr = hash.split('&');
        hash_arr.forEach((value,i) => {
            let val = hash_arr[i].split('=');
            hash_obj[encodeURI(val[0])] = encodeURI(val[1])
        })
    }
    return hash_obj
};

