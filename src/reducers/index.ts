import pageState, {IPageState} from "./pageState";
import {combineReducers} from "redux";
import todo, {ITodo} from "./todo";

const Store = combineReducers({
    pageState,
    todo
});

export type IStore = ReturnType<typeof Store>

export interface IState {
    pageState: IPageState,
    todo: ITodo[],
}
export default Store;