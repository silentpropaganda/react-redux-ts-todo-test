import {setIn} from 'immutable'
import {IPageStateActions, PageStateActionTypes} from "../actions/pageState";

const initialState:IPageState = {
    loading: true,
    selectedTodo: null,
    currentPage: 0,
    todoCount: 10,
    todoSearch: "",
    todoFilter: "all"
};

const pageState = (state = initialState,action:IPageStateActions):IPageState => {
    switch(action.type){
        case PageStateActionTypes.SET:
            return setIn(state,action.path,action.data);
        default:
            return state
    }
};

export interface IPageState {
    loading: boolean,
    selectedTodo: number | null,
    currentPage: number,
    todoCount: 10 | 20 | 50 | 100,
    todoSearch: string,
    todoFilter: TodoFilterTypes
}

export type TodoFilterTypes = "all" | "completed" | "due"

export default pageState