import {ITodoActions, TodoActionTypes} from "../actions/todo";

export interface ITodo {
    userId: number,
    id: number,
    title: string,
    completed: boolean
}
const initialState:ITodo[] = [];

export const todo = (state = initialState, action:ITodoActions):ITodo[] => {
    switch (action.type) {
        case TodoActionTypes.LOAD:
            return action.todo;
        default:
            return state
    }
};

export default todo;