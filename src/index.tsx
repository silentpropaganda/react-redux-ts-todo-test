import "@babel/polyfill";
import * as React from 'react';
import "./styles.scss";
import { render } from 'react-dom';
import { RootContainer } from './components/RootContainer';
import { createStore, applyMiddleware } from 'redux';
import Store, {IState} from "./reducers";
import { Provider } from 'react-redux';
import thunk from "redux-thunk";

export const store = createStore(Store,applyMiddleware(thunk));

render(
    <Provider store={store}>
        <RootContainer/>
    </Provider>,
    document.getElementById('app'),
);

