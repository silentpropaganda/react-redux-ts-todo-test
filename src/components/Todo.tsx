import * as React from "react";
import {ITodo} from "../reducers/todo";

interface IProps {
    todo: ITodo
    selectTodo(): void
}

const Todo:React.FC<IProps> = ({todo,selectTodo}) => {
    return (
        <div className={`todo ${todo.completed ? 'complete' : 'due'}`} onClick={selectTodo}>
            <div className="todo-title">
                {`${todo.id} ${todo.title}`}
            </div>
            <div className="todo-status">{todo.completed ? '✔' : '✘'}</div>
        </div>
    )
};

export default Todo