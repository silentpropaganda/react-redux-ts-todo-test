import * as React from "react";
import {TodoFilterTypes} from "../reducers/pageState";

interface IProps {
    setPageState(path: string[], data: any): void,
    todoFilter: TodoFilterTypes,
    currentPage: number,
    todoCount: number,
    todoSearch: string
    todoLength: number
}

const TodosHeader: React.FC<IProps> = ({setPageState,todoFilter,currentPage,todoCount,todoSearch,todoLength}) => {
    const options = [10, 20, 50, 100];
    const setSearch = (e:React.ChangeEvent<HTMLInputElement>) => {
        setPageState(['currentPage'],0);
        setPageState(['todoSearch'],e.target.value);
    };
    const setTodoCount = (e:React.ChangeEvent<HTMLSelectElement>) => {
        setPageState(['currentPage'],0);
        setPageState(['todoCount'],parseInt(e.target.value))
    };
    const setFilter = (e:React.ChangeEvent<HTMLSelectElement>) => {
        setPageState(['currentPage'],0);
        setPageState(['todoFilter'],e.target.value)
    };
    return (
        <div id="todo-bar">
            <input className="search"
                   value={todoSearch}
                   placeholder="Search in Todo"
                   onChange={setSearch}
            />
            <div className="todo-filter">
                <label>Filter </label>
                <select value={todoFilter}
                        onChange={setFilter}
                >
                    <option value="all">All</option>
                    <option value="completed">Completed</option>
                    <option value="due">Due</option>
                </select>
            </div>
            <div className="controls">
                <div className="count">
                    <label>Todos</label>
                    <select
                        value={todoCount}
                        onChange={setTodoCount}
                    >
                        {options.map(option => <option key={option} value={option}>{option}</option>)}
                    </select>
                </div>
                <div className="todo-length">
                    {"Todo count"}<br/>{todoLength}
                </div>
                <div className="page-switch">
                    <label>Page</label>
                    <div>
                        <button className="prev"
                                disabled={currentPage === 0}
                                onClick={() => setPageState(['currentPage'],0)}
                        >
                            {"❮❮"}
                        </button>
                        <button className="prev"
                            disabled={currentPage === 0}
                            onClick={() => setPageState(['currentPage'],currentPage - 1)}
                        >
                            {"❮"}
                        </button>
                        <div className="current-page">{currentPage + 1}</div>
                        <button className="next"
                            onClick={() => setPageState(['currentPage'],currentPage + 1)}
                            disabled={(currentPage + 1) * todoCount >= todoLength}
                        >{"❯"}</button>
                        <button className="next"
                                onClick={() => setPageState(['currentPage'],todoLength % todoCount > 0 ? Math.floor(todoLength / todoCount) : Math.floor(todoLength / todoCount) - 1)}
                                disabled={(currentPage + 1) * todoCount >= todoLength}
                        >{"❯❯"}</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default TodosHeader