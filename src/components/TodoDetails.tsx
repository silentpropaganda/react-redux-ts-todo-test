import * as React from "react";
import {ITodo} from "../reducers/todo";

interface IProps {
    todo?: ITodo,
    goBack(): void
}

const TodoDetails:React.FC<IProps> = ({todo,goBack}) => {
    if (!todo) return <div>Record not found</div>;
    return (
        <div className="todo-detail">
            <div className="properties" style={{backgroundColor: todo.completed ? "#49ca49" : "rgba(234, 194, 46, 0.98)"}}>
                {`${todo.id} - User: ${todo.userId}`}
                <span>{todo.completed ? 'COMPLETED' : 'DUE'}</span>
            </div>
            <p className="body">{todo.title}</p>
            <div onClick={() => goBack()} className="back pointer">BACK</div>
        </div>
    )
};


export default TodoDetails