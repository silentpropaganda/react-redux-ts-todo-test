import * as React from "react";
import {IState} from "../reducers";
import {connect} from "react-redux";
import {selectTodo, setPageState} from "../actions/pageState";
import {ITodo} from "../reducers/todo";
import Todos from "./Todos";
import TodoDetails from "./TodoDetails";
import {loadTodos} from "../actions/todo";
import {useEffect} from "react";
import {getHash} from "../utils";
import Loader from "./Loader";

interface StateToProps {
    todo: ITodo[],
    loading: boolean,
    selectedTodo: number | null
}

interface DispatchToProps {
    selectTodo(id: number | null, push?: boolean): void
    loadTodos(): Promise<void>,
    setPageState(path: string[], data: any): void
}

const RootComponent: React.FC<StateToProps & DispatchToProps> = ({todo, loading, selectedTodo, selectTodo, loadTodos, setPageState}) => {
    useEffect(() => {
        loadTodos()
            .then(() => {
                selectTodo(getHash(location.hash).todo || null, false);
                setPageState(['loading'],false);
            })
            .catch(() => {
                setPageState(['loading'], false);
            })
        const fun = () => selectTodo(getHash(location.hash).todo || null, false);
        window.addEventListener('popstate', fun);
        return () => window.removeEventListener('popstate', fun);

    }, []);
    if (loading) return <Loader/>;
    if (selectedTodo !== null) {
        return <div id="App">
            <TodoDetails
                todo={todo.find(todo => todo.id === selectedTodo)}
                goBack={() => selectTodo(null, true)}
            />
        </div>
    }
    return (
        <div id="App">
            <Todos/>
        </div>
    )
};

const states = (state: IState) => ({
    todo: state.todo,
    loading: state.pageState.loading,
    selectedTodo: state.pageState.selectedTodo
});

export const RootContainer = connect(states, {selectTodo, loadTodos, setPageState})(RootComponent);