import * as React from "react";
import { ITodo } from "../reducers/todo";
import Todo from "./Todo";
import {selectTodo, setPageState} from "../actions/pageState";
import { connect } from "react-redux";
import { IState } from "../reducers";
import TodosHeader from "./TodosHeader";
import {TodoFilterTypes} from "../reducers/pageState";

interface IProps {
}

interface StateToProps {
    todo: ITodo[],
    currentPage: number,
    todoCount: number,
    todoSearch: string,
    todoFilter: TodoFilterTypes
}

interface DispatchToProps {
    selectTodo(id: number,push?:boolean): void,
    setPageState(path: string[],data: any): void

}

const TodosComponent:React.FC<IProps & StateToProps & DispatchToProps> = ({todo,currentPage,todoCount,todoSearch,todoFilter,selectTodo,setPageState}) => {
    let filteredTodos = todoSearch !== "" ?
        todo.filter(todo => todo.title.includes(todoSearch) || todo.userId.toString() === todoSearch || todo.id.toString() === todoSearch) :
        todo;
    filteredTodos = todoFilter !== 'all' ? filteredTodos.filter(todo => todoFilter === "completed" ? todo.completed : !todo.completed) : filteredTodos;
    const todos = filteredTodos
        .slice(currentPage * todoCount,(currentPage * todoCount) + todoCount)
        .map((todo) => <Todo key={todo.id} todo={todo} selectTodo={() => selectTodo(todo.id)}/>);

    return (
        <div id="todos">
            <TodosHeader
                setPageState={setPageState}
                todoCount={todoCount}
                currentPage={currentPage}
                todoSearch={todoSearch}
                todoLength={filteredTodos.length}
                todoFilter={todoFilter}
            />
            <div id="todos-body">
            {todos}
            </div>
        </div>
    )
};

const states = (state:IState) => ({
    todo: state.todo,
    currentPage: state.pageState.currentPage,
    todoCount: state.pageState.todoCount,
    todoSearch: state.pageState.todoSearch,
    todoFilter: state.pageState.todoFilter
})

const Todos = connect(states,{setPageState,selectTodo})(TodosComponent);
export default Todos;