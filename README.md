# React-Redux-TS-Test

## Requirements

- Node >= 9

## Install

- `yarn install` or `npm install`

## Run in development

- To start in dev `yarn run start` or `npm run start`.
- The server will start at `localhost:8080` default.

## Run in production

- To build `yarn run build-prod` the output will be in the `./dist/` directory.
- You can host it with Nginx or any webserver.

### http-server example

- `npm install -g http-server@0.9.0` IMPORTANT, 
 this package is reportedly failing with the latest version, and this is the last known working release.
- `http-server ./dist/`

## Info

This app uses React, Redux, Redux-Thunk, Webpack, and Typescript.  
Just gets some JSON from an endpoint, and loads it to a simple framework,   
but could serve as a simple boilerplate for those who have problems to merge these technologies, as I had.
